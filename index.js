var EventEmitter = require('events').EventEmitter;
var vec3 = require('vec3');

module.exports = Init;

function Init()
{
return inject;
}

var bot;
var EntityToAttack = null;
var LastEntity = null;

function inject(_bot)
{
bot = _bot;
_bot.Knockback = new EventEmitter();
_bot.Knockback.enabled = true;
_bot.Knockback.endTime = 300; //300 ms
_bot.on('entitySwingArm', function (entity)
{
// We should ignore passive mobs
if (entity.kind == "Passive mobs" && !entity.player)
		return;
EntityToAttack = entity;
});

 _bot.on('entityHurt', function (entity) {
    // It doesn't really matter anymore if dead or disabled
    if (entity.health <= 0 || !_bot.Knockback.enabled)
        return;
    // Was it me?
    if (entity.username == bot.username)
         KnockbackFrom(EntityToAttack);
});
}

function KnockbackFrom(entity)
{
bot.Knockback.emit("KnockbackStart", entity);

// If we don't have a knockback direction just jump..
bot.setControlState('jump', true);
bot.setControlState('jump', false);

// We cannot react to an entity that doesn't exist.
if (entity == null)
    return;

// Stop current movement.
bot.clearControlStates();

// It's not perfect! Mainly because I wanted quick and simple knockback.
if (entity.position.x < bot.entity.position.x)
    bot.setControlState('right', true);
else if (entity.position.x > bot.entity.position.x)
    bot.setControlState('left', true);

if (entity.position.z < bot.entity.position.z)
    bot.setControlState('back', true);
else if (entity.position.z > bot.entity.position.z)
    bot.setControlState('forward', true);

// Clear this for the next time we get hit.
EntityToAttack = null;

// This will let the bot know knockback is over and the bot can do whatever it wants to react.
setTimeout(function() { bot.clearControlStates(); bot.Knockback.emit('KnockbackEnd', entity, bot.Knockback.endTime); }, bot.Knockback.endTime);
}
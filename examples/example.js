var KnockbackPlugin = require('../')(mineflayer); //Knockback plugin
var mineflayer = require('mineflayer');

var bot = mineflayer.createBot({
  host: 'localhost', // optional
  port: 25565,       // optional
  version:"1.10",
  username: "username",
  password: "password"
});

//Init bot plugins here
KnockbackPlugin(bot);

//Edit plugin vars here
bot.Knockback.enabled = true; // True by default
bot.Knockback.endTime = 300; // 300ms by default

bot.Knockback.on('KnockbackStart', function (entity){
  // The knockback function has just started.
});

bot.Knockback.on('KnockbackEnd', function (entity, time){
  // The knockback function has just ended resume previous tasks here.
  // Time should return the endTime value.
});

//-- Below this comment has nothing to do with the Knockback plugin --//
//-- Below this comment has nothing to do with the Knockback plugin --//
//-- Below this comment has nothing to do with the Knockback plugin --//

// The spawn event doesn't fire for me so this is a poor solution.
bot.on('login', function() {
  setInterval(function() {
    if (bot.entity != null)
      if (bot.health > 0)
      {
        // Fire spawn event and stop this timer.
        bot.emit('spawn');
        clearInterval(this);
      }
    }, 500); // Check every half a second.
});

// I've also had issues with respawning.
bot.on('death', function() {
  setTimeout(function() {
    setInterval(function() {
    if (bot.health <= 0)
    {
      console.log('Trying to respawn.');
      bot._client.write('client_command', { payload: 0 })
    }
    else
    {
      // Stop
      clearInterval(this);
    }
  }, 1500);
}, 500);
});